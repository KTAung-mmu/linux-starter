Linux server ဆိုင်ရာများကိုမျှဝေးပေးရန်နှင့် မတူညီသည့် distro များတွင်ဖြစ်လေ့ဖြစ်ထရှိသည့် အမှားများ၊ ပြင်ဆင်ချက်များကိုဝေမျှပေးရန်
openSUSE ကိုအခြေခံထားသော်လည်း CentOS, Ubuntu,Debian,Gentoo စသည့်  distro များကိုလည်းမျှဝေခြင်းလုပ်ဆောင်ပေးပါရန်
ဖြစ်လေ့ဖြစ်ထရှိသည့်အမှားများကိုစုစည်းဖော်ပြပေးပါရန်နှင့် စတင်လေ့လာကာစ လေ့လာသူများအတွက် wiki များကိုရေးသားပေးပါရန် 
